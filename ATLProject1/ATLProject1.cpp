﻿// ATLProject1.cpp: Implementierung von WinMain


#include "pch.h"
#include "framework.h"
#include "resource.h"
#include "ATLProject1_i.h"


using namespace ATL;

#include <stdio.h>

class CATLProject1Module : public ATL::CAtlServiceModuleT< CATLProject1Module, IDS_SERVICENAME >
{
public :
	DECLARE_LIBID(LIBID_ATLProject1Lib)
	DECLARE_REGISTRY_APPID_RESOURCEID(IDR_ATLPROJECT1, "{2e18c43a-96c5-4642-8033-5ed6f495fefb}")
	HRESULT InitializeSecurity() throw()
	{
		// TODO : CoInitializeSecurity aufrufen und die entsprechenden Sicherheitseinstellungen für den Dienst angeben
		// Empfohlen – Authentifizierung auf PKT-Ebene,
		// Identitätswechselebene RPC_C_IMP_LEVEL_IDENTIFY
		// und passenden Nicht-NULL-Sicherheitsdeskriptor hinzufügen.

		return S_OK;
	}
};

CATLProject1Module _AtlModule;



//
extern "C" int WINAPI _tWinMain(HINSTANCE /*hInstance*/, HINSTANCE /*hPrevInstance*/,
								LPTSTR /*lpCmdLine*/, int nShowCmd)
{
	return _AtlModule.WinMain(nShowCmd);
}

